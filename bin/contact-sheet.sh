#!/usr/bin/sh

# ImageMagick must be present and on $PATH

COMMAND=$(which montage)

if [ ! -x "${COMMAND}" ]
then
  printf "\nFILE NOT FOUND: montage\n\tImageMagick must be installed and on \$PATH\n"
  exit 1
fi

for arg in "$@"
do
  case $arg in
    -h|--help)
      printf "USAGE: \n\t%s --output=output-file filename-pattern\n" "$0"
      exit 0
      ;;
    -o=*|--output=*)
      OUTPUT="${arg#*=}"
      shift
      ;;
  esac
done

"${COMMAND}" -geometry 100x+100+4 -define jpeg:size=100x -tile 4x -font Bookerly -pointsize 16 -label "%f" "$@" "${OUTPUT}"

