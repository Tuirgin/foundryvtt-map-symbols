#!/usr/bin/sh

# Inkscape must be present and on $PATH

DATESTAMP=$(date +%Y%m%d%H%M%S)
ERROR_LOG="$DATESTAMP-errors.log"
SEPARATOR="--------------------------------------------------------------------------------"

COMMAND=$(which inkscape)
SIZES="16 32 64 128 256"

if [ ! -x "${COMMAND}" ]
then
  printf "\nFILE NOT FOUND: inkscape\n\tInkscape must be installed and on \$PATH\n"
  exit 1
fi

for arg in "$@"
do
  case $arg in
    -h|--help)
      printf "USAGE: \n\t%s --output=output-directory filename-pattern\n" "$0"
      exit 0
      ;;
    -o=*|--output=*)
      OUTPUT_PATH="${arg#*=}"
      shift
      if [ ! -d "${OUTPUT_PATH}" ]
      then
        printf "\nNOT A SIZE_DIRECTORY: %s" "${OUTPUT_PATH}"
      fi
      ;;
  esac
done

for FILENAME in "$@"
do
  for SIZE in $SIZES
  do
    if [ ${#SIZE} -eq 2 ]
    then
      SIZE_DIRECTORY="0$SIZE"
    else
      SIZE_DIRECTORY="$SIZE"
    fi

    SOURCE_ABSOLUTE_PATH=$(readlink -f "${FILENAME}")
    SOURCE_BASENAME=$(basename "${SOURCE_ABSOLUTE_PATH}")
    CATEGORY_DIRECTORY=$(basename "$(dirname "${SOURCE_ABSOLUTE_PATH}")")
    EXPORT_FILENAME="${SOURCE_BASENAME%.svg}-${SIZE}.png"
    EXPORT_DIRECTORY="${OUTPUT_PATH}/${CATEGORY_DIRECTORY}/${SIZE_DIRECTORY}"
    EXPORT_PATH="${EXPORT_DIRECTORY}/${EXPORT_FILENAME}"

    if [ ! -d "${EXPORT_DIRECTORY}" ]
    then
      mkdir -p "${EXPORT_DIRECTORY}"
    fi

    # Check our Variables
    # echo "\$COMMAND: $COMMAND"
    # echo "\$FILENAME: $FILENAME"
    # echo "\$SIZE: $SIZE"
    # echo "\$SIZE_DIRECTORY: $SIZE_DIRECTORY"
    # echo "\$SOURCE_ABSOLUTE_PATH: ${SOURCE_ABSOLUTE_PATH}"
    # echo "\$SOURCE_BASENAME: $SOURCE_BASENAME"
    # echo "\$CATEGORY_DIRECTORY: $CATEGORY_DIRECTORY"
    # echo "\$EXPORT_FILENAME: $EXPORT_FILENAME"
    # echo "\$EXPORT_DIRECTORY: $EXPORT_DIRECTORY"
    # echo "\$EXPORT_PATH: $EXPORT_PATH"

    echo $SEPARATOR
    echo "Source File: ${SOURCE_ABSOLUTE_PATH}"
    echo "Output File: ${EXPORT_PATH}"

    ${COMMAND} \
      --export-area-page \
      --export-type=png \
      --export-width="${SIZE}" \
      --export-height="${SIZE}" \
      --export-background-opacity=0.0 \
      --export-type=png \
      --export-filename="${EXPORT_PATH}" \
      "${FILENAME}" 2> "${ERROR_LOG}"
  done
done
