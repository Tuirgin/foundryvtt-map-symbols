# README

## What's Here?

This is a collection of SVG images consisting primarily of alpha-numeric glyphs and cartography symbols. From these PNG images have been rendered at several target sizes, which should accommodate most uses. The numeric folder names with the PNG categories indicate the size of the images; i.e. "016" indicates 16x16 images, while "256" indicates 256x256 images.

Contact sheets can be found within the `docs/contact_sheets` directory.

The `bin` directory contains shell scripts useful for generating the contact sheets and for rendering the PNG images.

### Directory Listing

    foundryvtt-map-symbols
    ├── assets
    │   ├── png
    │   │   ├── alpha
    │   │   │   ├── 016
    │   │   │   ├── 032
    │   │   │   ├── 064
    │   │   │   ├── 128
    │   │   │   └── 256
    │   │   ├── cartography
    │   │   │   ├── 016
    │   │   │   ├── 032
    │   │   │   ├── 064
    │   │   │   ├── 128
    │   │   │   └── 256
    │   │   ├── digits
    │   │   │   ├── 016
    │   │   │   ├── 032
    │   │   │   ├── 064
    │   │   │   ├── 128
    │   │   │   └── 256
    │   │   └── punctuation
    │   │       ├── 016
    │   │       ├── 032
    │   │       ├── 064
    │   │       ├── 128
    │   │       └── 256
    │   └── svg
    │       ├── alpha
    │       ├── alpha-accents
    │       ├── cartography
    │       ├── digits
    │       └── punctuation
    ├── bin
    ├── docs
    │   └── contact_sheets
    └── src

    36 directories
